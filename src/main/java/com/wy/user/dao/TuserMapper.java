package com.wy.user.dao;

import com.wy.user.entity.Tuser;

import java.util.List;

/**
 * TODO
 * Created by wy on 2021/11/12
 */
public interface TuserMapper{
    Tuser selectTuserByUsername(String username);
    List<Tuser> getAll();
}
