package com.wy.user.service;

import com.github.pagehelper.PageInfo;
import com.wy.user.entity.Tuser;

/**
 * TODO
 * Created by wy on 2021/11/12
 */
public interface TuserService {
    Tuser login(String username, String password);
    PageInfo<Tuser> getByPage(int pageIndex);
}
