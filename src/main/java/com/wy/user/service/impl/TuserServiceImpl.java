package com.wy.user.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wy.user.dao.TuserMapper;
import com.wy.user.entity.Tuser;
import com.wy.user.service.TuserService;
import com.wy.user.util.MybatisUtils;

import java.util.List;

/**
 * TODO
 * Created by wy on 2021/11/12
 */
public class TuserServiceImpl implements TuserService {
    TuserMapper mapper = MybatisUtils.getMapper(TuserMapper.class);




    @Override
    public Tuser login(String username, String password) {
        Tuser tuser =  mapper.selectTuserByUsername(username);

        if(tuser!=null){
           if(tuser.getPassword().equals(password)){
               return tuser;
           }
        }
        return null;
    }

    @Override
    public PageInfo<Tuser> getByPage(int pageIndex) {
        //开启分页功能
        PageHelper.startPage(pageIndex,5);
        //查询所有
        List<Tuser> tusers =  mapper.getAll();



        //返回分页信息加上当前类
        PageInfo<Tuser> tuserInfo = new PageInfo<>(tusers);
        return tuserInfo;
    }
}
