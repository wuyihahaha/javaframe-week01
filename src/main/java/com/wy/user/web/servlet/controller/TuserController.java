package com.wy.user.web.servlet.controller;

import com.github.pagehelper.PageInfo;
import com.wy.user.common.cons.Const;
import com.wy.user.entity.Tuser;
import com.wy.user.service.TuserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TODO
 * Created by wy on 2021/11/12
 */
@WebServlet("/tuser")
public class TuserController extends BaseServlet{
    ApplicationContext ac = new ClassPathXmlApplicationContext("spring-config.xml");
    TuserService ts = (TuserService) ac.getBean("ts");

    public String login(HttpServletRequest req, HttpServletResponse resp){
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        Tuser login = ts.login(username, password);
        System.out.println(login);
        if(login!=null){

            req.getSession().setAttribute(Const.IS_LOGIN, login);
            return Const.FORWADRD+"/tuser?method=showByPage";
        }else {
            req.setAttribute("msg", "用户名或者密码错误");
            return Const.FORWADRD+"login.jsp";
        }
    }

    public String register(HttpServletRequest req, HttpServletResponse resp){
        return null;
    }

    public String showByPage(HttpServletRequest req, HttpServletResponse resp){
        String pageIndex = req.getParameter("pageIndex");
        if(pageIndex==null || pageIndex.equals("")){
            pageIndex="1";
        }
        PageInfo<Tuser> pageInfos = ts.getByPage(Integer.valueOf(pageIndex));
        req.setAttribute("pageInfos",pageInfos);

        return Const.FORWADRD+"index.jsp";
    }
}
