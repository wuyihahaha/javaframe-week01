package com.wy.user.web.servlet.controller;





import com.wy.user.common.cons.Const;
import com.wy.user.util.StrUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

//作为父类被继承
public class BaseServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //0 处理method 是null 或者 ""
        String method = request.getParameter("method");
        if (!StrUtils.isNotEmpty(method)) {
            response.sendRedirect(request.getContextPath()+"/login.jsp");
            //不让方法继续向下执行
            return;
        }
        try {
            //当前对象
            Class<? extends BaseServlet> aClass = this.getClass();
            //找到对应的方法
            Method me = aClass.getMethod(method, HttpServletRequest.class, HttpServletResponse.class);
            //反射调用方法
            String invoke = (String) me.invoke(this, request, response);
            //4 把页面的响应处理交他完成
            // 1.forward   2.redirect 3.json字符串
            if (invoke!=null) {
                if(invoke.startsWith(Const.FORWADRD)){
                    //要求转发
                    //直接丢进去一个标识符uri
                    request.getRequestDispatcher(invoke.substring(invoke.indexOf(":")+1)).forward(request, response);

                }else if(invoke.startsWith(Const.READIRECT)){
                    //要求重定向
                    response.sendRedirect(request.getContextPath()+"/"+invoke.substring(invoke.indexOf(":")+1));
                }else{
                    //json字符串
                    PrintWriter writer = response.getWriter();
                    writer.write(invoke);
                    writer.flush();
                    writer.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect(request.getContextPath()+"/error/error.jsp");
        }
    }
}
