package com.wy.user.entity;


import java.util.Date;

public class Tuser {

  private int id;
  private String username;
  private String password;
  private String sex;
  private Date birthday;

  public Date getBirthday() {
    return birthday;
  }

  public Date getRegistTime() {
    return registTime;
  }

  @Override
  public String toString() {
    return "Tuser{" +
            "id=" + id +
            ", username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", sex='" + sex + '\'' +
            ", birthday=" + birthday +
            ", registTime=" + registTime +
            '}';
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public void setRegistTime(Date registTime) {
    this.registTime = registTime;
  }

  private Date registTime;


  public int getId() {
    return id;
  }

  public Tuser() {
  }

  public void setId(int id) {
    this.id = id;
  }


  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }


  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }



  public void setBirthday(java.sql.Timestamp birthday) {
    this.birthday = birthday;
  }



  public void setRegistTime(java.sql.Timestamp registTime) {
    this.registTime = registTime;
  }

}
