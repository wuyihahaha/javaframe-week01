package com.wy.user.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;


public class MybatisUtils {

    private static SqlSessionFactory sf;
    private static final ThreadLocal<SqlSession> THREAD_LOCAL = new ThreadLocal<SqlSession>();

    static {

        try {
            InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
            sf = new SqlSessionFactoryBuilder().build(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static SqlSession getSession(){
        SqlSession session = THREAD_LOCAL.get();
        if (session == null) {
            session=sf.openSession();
            THREAD_LOCAL.set(session);
        }
        return session;
    }

    public static void close(){
        SqlSession session = getSession();
        session.close();
        THREAD_LOCAL.remove();
    }

    public static void commit(){
        getSession().commit();
        close();
    }

    public static void rollback(){
        getSession().rollback();
        close();
    }

    public static <T> T getMapper(Class<T> clazz){
        return getSession().getMapper(clazz);
    }

}
