<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2021/11/12
  Time: 16:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>登录</title>
    <link rel="stylesheet" type="text/css" href="css/login2.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.js"></script>
</head>
<body>
<div class="col-md-6 col-md-offset-3" style="border: 1px black solid">
    <form  method="post" action="${ctx}/tuser?method=login" class="form center" id="userLogin" >

            <div class="form-group" style="text-align: center;font-size: 40px;font-weight: bold">
                登录
            </div>
            <div class="form-group">
                <label >username</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="Password">
            </div>
             <div class="form-group">
                 <label >password</label>
                 <input type="password" class="form-control" id="password" name="password" placeholder="Password">
             </div>
            <button type="submit" class="btn btn-default">Submit</button>

    </form>
</div>


</body>
</html>
