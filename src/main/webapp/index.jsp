<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2021/11/10
  Time: 17:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 添加jstl -->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>分页功能显示</title>
    <link rel="stylesheet" type="text/css" href="css/login2.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.js"></script>
</head>
<body>
<div class="col-md-offset-1 col-md-10">
    <table class="table table-bordered table-striped ">
        <thead>
        <th>用户编号</th>
        <th>用户姓名</th>
        <th>用户密码</th>
        <th>用户性别</th>
        <th>用户生日</th>
        <th>用户注册日期</th>
        </thead>
        <tbody>
        <c:forEach items="${pageInfos.list}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.username}</td>
                <td>${user.password}</td>
                <td>${user.sex}</td>
                <td><f:formatDate value="${user.birthday}" pattern="yyyy-MM-dd HH:mm:ss"/>  </td>                          </td>

                <td><f:formatDate value="${user.registTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
            </tr>
        </c:forEach>
        </tbody>
        <tfoot >
        <tr>
            <td colspan="7">
                <nav aria-label="Page navigation" style="text-align: center">
                    <ul class="pagination">
                        <li><a href="${ctx}/tuser?method=showByPage&pageIndex=1">首页</a></li>
                        <li>
                            <a href="${ctx}/tuser?method=showByPage&pageIndex=${pageInfos.pageNum-1}" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <c:forEach items="${pageInfos.navigatepageNums}" var="page">
                            <li><a href="${ctx}/tuser?method=showByPage&pageIndex=${page}">${page}</a></li>
                        </c:forEach>

                        <li>
                            <a href="${ctx}/tuser?method=showByPage&pageIndex=${pageInfos.nextPage}" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                        <li><a href="${ctx}/tuser?method=showByPage&pageIndex=${pageInfos.pages}">尾页</a></li>
                    </ul>
                </nav>
                <%--                    <a href="${ctx}/tuser?method=showByPage&pageIndex=1">首页</a>--%>
                <%--                    <a href="${ctx}/tuser?method=showByPage&pageIndex=${pageInfos.pageNum-1}">上一页</a>--%>
                <%--                    <a href="${ctx}/tuser?method=showByPage&pageIndex=${pageInfos.nextPage}">下一页</a>--%>
                <%--                    <a href="${ctx}/tuser?method=showByPage&pageIndex=${pageInfos.pages}">尾页</a>--%>
                <%--                    <c:forEach items="${pageInfos.navigatepageNums}" var="page">--%>
                <%--                        <a href="${ctx}/tuser?method=showByPage&pageIndex=${page}">${page}</a>--%>
                <%--                    </c:forEach>--%>
                <%--                    <input type="text" readonly value="${pageInfos.pageNum}/${pageInfos.pages}" style="width: 50px">--%>
            </td>
        </tr>
        </tfoot>
    </table>
</div>

</body>
</html>
